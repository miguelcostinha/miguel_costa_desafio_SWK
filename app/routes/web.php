<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return "Hello from swk dev team";
    }
);

$router->group(['prefix' => 'api'], function () use ($router) {
    //$router->get('/videos',  ['uses' => 'ControllerVideos@showAllVideos']);
  
    $router->get('/videos', ['uses' => 'ControllerVideos@showvideosyt']);
  
    $router->post('/videos', ['uses' => 'ControllerVideos@create']);
  
    $router->delete('/video/{id}', ['uses' => 'ControllerVideos@delete']);
  
    $router->put('/video/{id}', ['uses' => 'ControllerVideos@update']);

    $router->get('/video/getvideo/{id}', ['uses' => 'ControllerVideos@getUrl']);
  });
