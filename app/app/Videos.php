<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{

    protected $table = 'videosyoutube';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','description','url', 'youtuber'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = [];
}