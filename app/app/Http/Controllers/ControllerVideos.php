<?php

namespace App\Http\Controllers;

use App\Videos;
use Illuminate\Http\Request;

class ControllerVideos extends Controller
{

    public function showvideosyt()
    {
        $video = Videos::all();
        return response()->json($video);
    }
    /*public function showAllVideos()
    {
        return response()->json(Videos::all());
    }

    public function showOneVideo($id)
    {
        return response()->json(Videos::find($id));
    }*/

    public function create(Request $request )
    {
        $this->validate($request, [
            
            'title' => 'required',
            'description' => 'required',
            'url' => 'required'

        ]);
        $video = Videos::create($request->all());

        return response()->json($video, 201);

    }

    public function update($id, Request $request)
    {
        $video= Videos::find($id);

        $video->title = $request->input('title');
        $video->description = $request->input('description');
        $video->save();
        return response()->json($video);
    }

    public function delete($id)
    {
        Videos::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function getUrl($urlvideo) {
    
    $url= 'https://www.youtube.com/channel?id_channel='.$urlvideo;
    $xml_url = simplexml_load_file($url);

    return response()->json($url);
    }
}